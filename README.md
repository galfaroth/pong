# README #

This is an example Pong-style game written in Unity3D using Photon Network. The project will come in stages:
* 1. One player game
* 2. Two players local game
* 3. Two players match-making multiplayer game
* 4. Ability to customize game assets remotely.