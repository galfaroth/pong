﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reskinner : MonoBehaviour {
	public List<UnityEngine.UI.Image> imagesToReplace; //List of images that need to be skinned
	public string hostURL = "http://www.polydust.com/pong/";

	static string kCurrentBundle = "current-bundle";

	void Start() {
		StartCoroutine(ShowCurrentBundle());
		StartCoroutine(DownloadCurrentBundleName());
	}
	IEnumerator DownloadCurrentBundleName(){
		WWW www = new WWW(hostURL+"current-asset.txt"); //The content of this .tx.t has string: christmas or bat and asset bundles to download within the same folder
        yield return www;
		PlayerPrefs.SetString(kCurrentBundle, www.text); //Save the currently saved asset bundle for faster boot next time
		ShowCurrentBundle();
	}
	IEnumerator ShowCurrentBundle(){
		string bundleName = PlayerPrefs.GetString(kCurrentBundle);
		if(bundleName != null && bundleName.Length>0){
			string assetPath = hostURL+bundleName;

			while (!Caching.ready) //Wait for the Caching system to be ready
				yield return null;

			//Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
			using(WWW www2 = WWW.LoadFromCacheOrDownload (assetPath, 1)){
				yield return www2;
				if (www2.error != null)
					throw new Exception("WWW download had an error:" + www2.error);
				AssetBundle bundle = www2.assetBundle;
				var iconTexture = bundle.LoadAsset(bundleName) as Texture2D;
				var iconRect = new Rect(0, 0, 256,256);
				var iconSprite = Sprite.Create(iconTexture, iconRect, new Vector2(.5f, .5f));
				
				foreach(UnityEngine.UI.Image image in imagesToReplace){
					image.sprite = iconSprite;
					image.enabled = true;
				}
				//Unload the AssetBundles compressed contents to conserve memory
				bundle.Unload(false);

			} //Memory is freed from the web stream (www.Dispose() gets called implicitly)
		}
	}
}
