﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void BouncedEvent(GameObject sender, EventArgs e);

public class Ball : MonoBehaviour, IPunObservable {
	#region ----Properties----
    public event BouncedEvent OnBounce; //Public event that helps to determine when the bouncing occured (useful for changing ball permissions)
	public bool multiplayerMode = false; //Used to determine if the ball should be simulated locally or read from the server (sometimes)
	public Vector2 velocity {
		get { 
			return _velocity;
		}
	}
	Vector2 _velocity;
	
	[SerializeField]
	GameplaySettings gameplaySettings;
	GameObject bouncedPlayer = null;
	#endregion

	#region ----Public Methods----
	public void Stop(){
		_velocity = Vector3.zero;
	}
	public void Serve(Player.Side playerIndex){
		_velocity = new Vector2(playerIndex == Player.Side.Left ? -gameplaySettings.ballServeVelocity : gameplaySettings.ballServeVelocity, 0);
	}
	#endregion

	#region ----Physics----
	void FixedUpdate () {
        if (!multiplayerMode || (multiplayerMode && this.GetComponent<PhotonView>().isMine)){
			transform.Translate(velocity * Time.deltaTime, Space.World);
        }
	}
	void OnTriggerEnter2D(Collider2D other){
		_velocity = Vector2.Reflect(velocity, other.transform.up); //All collision objects must be oriented in Y-axis to face the play area

		Player player = other.GetComponent<Player>();
		if(player != null) { //Paddles have a script called Player which contains their velocity
			_velocity += new Vector2(0,player.input.verticalVelocity*gameplaySettings.ballPlayerBounceVelocityMultiplier);
			bouncedPlayer = player.gameObject;
		}
	}
	#endregion

	#region ----Networking Layer----
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if (stream.isWriting){ //We own this player: send the others our data
			stream.SendNext(velocity);
			if(bouncedPlayer != null){
				OnBounce(bouncedPlayer,EventArgs.Empty);//We want to send the bounce event only when the first velocity was set
				bouncedPlayer = null;
			}
		} else { //Network player, receive data
			_velocity = (Vector2)stream.ReceiveNext();
		}
	}
	#endregion
}
