﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void MoveEvent(object sender, EventArgs e);

public class PlayerInput : MonoBehaviour {
	#region ----Public Enums----
	public enum InputType{
		OnePlayer = 0,
		TwoPlayerLeft,
		TwoPlayerRight,
		Multiplayer
	}
	#endregion

	#region ----Properties----
    public event MoveEvent OnMove; //Public Event fired when a player moves (useful to initiate serving)
	public InputType inputType;
	public Transform servingPosition{
		get{
			return _servingPosition;
		}
	}
	[SerializeField]
	Transform _servingPosition;

	public float verticalVelocity { 
		get{
			return _verticalVelocity;
		}
	}
	float _verticalVelocity;

	Vector3 initialPosition; //Useful to reset the player back to middle
	[SerializeField]
	GameplaySettings gameplaySettings;
	#endregion

	void Start(){
		initialPosition = transform.position;
	}

	#region ----Public Methods----
	public void ResetPosition(){
		transform.position = initialPosition;
	}
	#endregion

	#region ----Feeling----
	void FixedUpdate () {
		if(inputType == InputType.OnePlayer || (inputType == InputType.Multiplayer && GetComponent<PhotonView>().isMine)){
			if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)){
				_verticalVelocity += 2;
				OnMove(this,EventArgs.Empty);
			} else if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)){
				_verticalVelocity -= 2;
				OnMove(this,EventArgs.Empty);
			} else {
				_verticalVelocity = 0;
			}
		} else {
			if(inputType == InputType.TwoPlayerLeft){
				if(Input.GetKey(KeyCode.W)){
					_verticalVelocity += 2;
					OnMove(this,EventArgs.Empty);
				} else if(Input.GetKey(KeyCode.S)){
					_verticalVelocity -= 2;
					OnMove(this,EventArgs.Empty);
				} else {
					_verticalVelocity = 0;
				}
			} else {
				if(Input.GetKey(KeyCode.UpArrow)){
					_verticalVelocity += 2;
					OnMove(this,EventArgs.Empty);
				} else if(Input.GetKey(KeyCode.DownArrow)){
					_verticalVelocity -= 2;
					OnMove(this,EventArgs.Empty);
				} else {
					_verticalVelocity = 0;
				}
			}
		}
		_verticalVelocity = Mathf.Clamp(_verticalVelocity, -gameplaySettings.playerMaxVerticalVelocity, gameplaySettings.playerMaxVerticalVelocity);
		transform.position = new Vector3(transform.position.x, transform.position.y + _verticalVelocity);

		//Clamp the movement not to cross the borders
		GetComponent<RectTransform>().anchoredPosition = new Vector2(
			GetComponent<RectTransform>().anchoredPosition.x, Mathf.Clamp(GetComponent<RectTransform>().anchoredPosition.y, 
			-gameplaySettings.playerYRange,gameplaySettings.playerYRange));
	}
	#endregion
}
