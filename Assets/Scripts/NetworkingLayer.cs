﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void RoomReady(object sender, EventArgs e);
public delegate void RoomNotReady(object sender, EventArgs e);

public class NetworkingLayer : Photon.PunBehaviour {
	#region ----Properties----
	//Public events that help to determine what's the matchmaking status
	public event RoomReady OnRoomReady;
	public event RoomNotReady OnRoomNotReady;

	//Gameplay Controlled by the Networking Layer
	[SerializeField]
	Gameplay gameplay; 
	
	//Internal bools helping to deal with the unusual situations
	bool waitingForPlayers = true;
	bool shouldJoinAnyRoom = false;
	bool inLobby = false;
	#endregion
	
    void Start(){
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

	#region ----Public Methods----
	public void FindGameAndJoin(){
		shouldJoinAnyRoom = true; //We use this flag to automatically join room after getting into the lobby
		if(inLobby){ //If we are in lobby, join now!
			shouldJoinAnyRoom = false;
			PhotonNetwork.JoinRandomRoom();
		}
	}
	public void LeaveGame(){
		inLobby = false;
		waitingForPlayers = true;
		shouldJoinAnyRoom = false;
		PhotonNetwork.Disconnect(); //This was the most reliable way to reset networking
	}
	#endregion

	#region ----Game Specific----
	IEnumerator AssignPlayer(){ //Assign player permissions
		yield return new WaitForSeconds(.5f);
		if(gameplay.playerLeft.GetComponent<PhotonView>().ownerId == 0){ //If there is no left player, we will become it
			gameplay.ball.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
			gameplay.playerLeft.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
		} else { //We become the right player
			gameplay.playerRight.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
		}
		CheckGameReady();
	}
	void CheckGameReady(){
		bool prevWaiting = waitingForPlayers;
		waitingForPlayers = (
			gameplay.playerLeft.GetComponent<PhotonView>().ownerId == 0 || 
			gameplay.playerRight.GetComponent<PhotonView>().ownerId == 0 || 
			gameplay.playerLeft.GetComponent<PhotonView>().ownerId == gameplay.playerRight.GetComponent<PhotonView>().ownerId);

		if(prevWaiting != waitingForPlayers){ //Check if the state changed
			if(waitingForPlayers){
				OnRoomNotReady(this,EventArgs.Empty);
				gameplay.ball.Stop();
			} else {
				OnRoomReady(this,EventArgs.Empty);
				gameplay.ResetGame();
			}
		}
	}
	#endregion
	
	#region ----Matchmaking----
	void CreateRandomRoom(){
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.IsVisible = false;
		roomOptions.MaxPlayers = 2;
		roomOptions.IsVisible = true;
		roomOptions.IsOpen = true;
		PhotonNetwork.JoinOrCreateRoom(Guid.NewGuid().ToString(), roomOptions, TypedLobby.Default);
	}
	public override void OnConnectedToMaster(){
		base.OnConnectedToMaster();
		PhotonNetwork.JoinLobby();
	}
	public override void OnJoinedLobby(){
		base.OnJoinedLobby();
		inLobby = true;
		if(shouldJoinAnyRoom){
			shouldJoinAnyRoom = false;
			PhotonNetwork.JoinRandomRoom();
		}
	}
	public override void OnPhotonRandomJoinFailed(object[] codeAndMsg){
		base.OnPhotonRandomJoinFailed(codeAndMsg);
		CreateRandomRoom();
	}
	public override void OnPhotonJoinRoomFailed(object[] codeAndMsg){
		base.OnPhotonJoinRoomFailed(codeAndMsg);
		CreateRandomRoom();
	}
	public override void OnJoinedRoom(){
		base.OnJoinedRoom();
		StartCoroutine(AssignPlayer());
	}
	public override void OnOwnershipTransfered(object[] viewAndPlayers){
		base.OnOwnershipTransfered(viewAndPlayers);
		CheckGameReady();
	}
	public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer){
		base.OnPhotonPlayerConnected(newPlayer);
		CheckGameReady();
	}
	public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer){
		base.OnPhotonPlayerDisconnected(otherPlayer);
		CheckGameReady();
	}
	public override void OnDisconnectedFromPhoton(){
		base.OnDisconnectedFromPhoton();
        PhotonNetwork.ConnectUsingSettings("0.1");
	}
	#endregion
}
