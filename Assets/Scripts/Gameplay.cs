﻿using UnityEngine;
using System;
using UnityEngine.UI;


public class Gameplay : MonoBehaviour {
	#region ----Properties----
	//Public properties
	public Game.State gameState;

	//Public getters
	public Player playerLeft{
		get{
			return _playerLeft;
		}
	}
	[SerializeField]
	Player _playerLeft;

	public Player playerRight{
		get{
			return _playerRight;
		}
	}
	[SerializeField]
	Player _playerRight;

	public Ball ball{
		get{
			return _ball;
		}
	}
	[SerializeField]
	Ball _ball;

	//Private properties
	Player.Side nextServer = Player.Side.None;
	[SerializeField]
	TriggerableArea leftTrigger;
	[SerializeField]
	TriggerableArea rightTrigger;

	[SerializeField]
	RectTransform wallRect;

	[SerializeField]
	Text player1Text;
	[SerializeField]
	Text player2Text;
	[SerializeField]
	Text gameOverText;

	[SerializeField]
	RectTransform gameOverCanvas;
	[SerializeField]
	GameplaySettings gameSettings;
	#endregion

	void Start () {
		leftTrigger.OnCollide += (object sender, EventArgs e) => {
			TriggerCollided(Player.Side.Left);
		};
		rightTrigger.OnCollide += (object sender, EventArgs e) => {
			TriggerCollided(Player.Side.Right);
		};
		playerLeft.input.OnMove += (object sender, EventArgs e) => {
			PlayerMoved(Player.Side.Left);
		};
		playerRight.input.OnMove += (object sender, EventArgs e) => {
			PlayerMoved(Player.Side.Right);
		};
		playerLeft.OnWon += (object sender, EventArgs e) => {
			ShowGameOver(true);
		};
		playerRight.OnWon += (object sender, EventArgs e) => {
			ShowGameOver(true);
		};
		ball.OnBounce += (GameObject sender, EventArgs e) => {
			//Transfer the ball's ownership to next player after bounce and first velocity update
			if(gameState == Game.State.MultiplayerReady && sender.GetComponent<PhotonView>().isMine){
				if(sender.GetComponent<Player>() == playerLeft){
					ball.GetComponent<PhotonView>().TransferOwnership(playerRight.GetComponent<PhotonView>().ownerId);
				} else {
					ball.GetComponent<PhotonView>().TransferOwnership(playerLeft.GetComponent<PhotonView>().ownerId);
				}
			}
		};
	}
	void Update(){
		UpdateScores(); //We want to have the most recent scores updated from the Player models
	}

	#region ----Public Methods----
	public void StartGame(Game.State gameplayType){
		this.gameState = gameplayType;
		ball.multiplayerMode = false;
		nextServer = Player.Side.Left;
		if(gameplayType == Game.State.OnePlayer){
			wallRect.gameObject.SetActive(true);
			playerRight.gameObject.SetActive(false);
			playerLeft.input.inputType = PlayerInput.InputType.OnePlayer;
			player1Text.gameObject.SetActive(false);
			player2Text.gameObject.SetActive(false);
			rightTrigger.gameObject.SetActive(false);
		} else {
			rightTrigger.gameObject.SetActive(true);
			player1Text.gameObject.SetActive(true);
			player2Text.gameObject.SetActive(true);
			ResetGame();
			playerRight.gameObject.SetActive(true);
			wallRect.gameObject.SetActive(false);
			if(gameplayType == Game.State.TwoPlayers){
				playerLeft.input.inputType = PlayerInput.InputType.TwoPlayerLeft;
				playerRight.input.inputType = PlayerInput.InputType.TwoPlayerRight;
			} else if (gameplayType == Game.State.MultiplayerReady){
				playerLeft.input.inputType = PlayerInput.InputType.Multiplayer;
				playerRight.input.inputType = PlayerInput.InputType.Multiplayer;
				ball.multiplayerMode = true;
			}
		}
		PlaceBallForServer();
	}
	public void ResetGame(){
		ball.transform.position = PlayerForIndex(Player.Side.Left).input.servingPosition.position;
		if(gameState == Game.State.MultiplayerReady && PhotonNetwork.connected)
			ball.GetComponent<PhotonView>().TransferOwnership(playerLeft.GetComponent<PhotonView>().ownerId);
		nextServer = Player.Side.Left;
		playerLeft.score = 0;
		playerRight.score = 0;
		ResetPlayerPositions();
		ball.Stop();
		PlaceBallForServer();
	}
	public Player PlayerForIndex(Player.Side playerIndex){
		if(playerIndex == Player.Side.Left){
			return playerLeft;
		} else {
			return playerRight;
		}
	}
	#endregion

	#region ----Gameplay specific----
	void PlayerMoved(Player.Side playerSide){
		ShowGameOver(false); //Always hide game over when moving player
		if((playerSide == Player.Side.Left && nextServer == Player.Side.Left) || (playerSide == Player.Side.Right && nextServer == Player.Side.Right)){
			ball.Serve(nextServer);
			nextServer = Player.Side.None;
		}
	}
	void TriggerCollided(Player.Side triggerSide){
		if(gameState == Game.State.MultiplayerReady){ //We want to send this as RPC for all players to have the synced version of the gameplay
			PhotonView photonView = PhotonView.Get(this); 
			photonView.RPC("PointLost", PhotonTargets.All, triggerSide==Player.Side.Left);
		} else {
			PointLost(triggerSide==Player.Side.Left);
		}
	}
	[PunRPC]
	void PointLost(bool isLeftTrigger){
		Player.Side playerIndex = Player.Side.Left;
		if(isLeftTrigger){
			playerIndex = Player.Side.Right;
		}
		PlayerForIndex(playerIndex).IncrementScore();
		ball.Stop();
		nextServer = playerIndex == Player.Side.Left ? Player.Side.Right : Player.Side.Left; //Change the serving player to next
		if(gameState == Game.State.MultiplayerReady)
			ball.GetComponent<PhotonView>().TransferOwnership(PlayerForIndex(nextServer).GetComponent<PhotonView>().ownerId);
		ResetPlayerPositions();
		PlaceBallForServer();
	}
	void ResetPlayerPositions(){
		PlayerForIndex(Player.Side.Left).input.ResetPosition();
		PlayerForIndex(Player.Side.Right).input.ResetPosition();
	}
	void PlaceBallForServer(){
		ball.transform.position = PlayerForIndex(nextServer).input.servingPosition.position;
	}
	#endregion

	#region ----Visual Updates----
	void UpdateScores(){
		player1Text.text = ""+playerLeft.score;
		player2Text.text = ""+playerRight.score;
	}
	void ShowGameOver(bool show){
		if(show){
			gameOverCanvas.gameObject.SetActive(true);
			gameOverText.text = PlayerForIndex(Player.Side.Left).score >= gameSettings.maxScore ? "Player left won" : "Player right won";
			ResetGame();
		} else {
			gameOverCanvas.gameObject.SetActive(false);
		}
	}
	#endregion

}
