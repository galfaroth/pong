﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplaySettings : MonoBehaviour {
	public float ballServeVelocity = 200;
	public float ballPlayerBounceVelocityMultiplier = 10;
	public float playerMaxVerticalVelocity = 8;
	public float playerYRange = 205;
	public int maxScore = 12;
}
