﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void EnteredCollision(object sender, EventArgs e);

public class TriggerableArea : MonoBehaviour {
    public event EnteredCollision OnCollide; //Public event informing that a collision has just happened

	void OnTriggerEnter2D(Collider2D other){
         OnCollide(this,EventArgs.Empty);
	}
}