﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Game : MonoBehaviour {
	#region ----Public Enums----
	public enum State{
		Menu = 0,
		OnePlayer,
		TwoPlayers,
		MultiplayerConnecting,
		MultiplayerReady
	}
	#endregion

	#region ----Properties----
	State gameState = State.Menu;
	[SerializeField]
	Canvas menuCanvas;
	[SerializeField]
	Canvas connectingCanvas;
	[SerializeField]
	Canvas gameplayCanvas;
	[SerializeField]
	NetworkingLayer networkingLayer;
	[SerializeField]
	Gameplay gameplay;
	#endregion

	void Start(){
		menuCanvas.gameObject.SetActive(true);
		networkingLayer.OnRoomReady += (object sender, EventArgs e) => {
			gameState = State.MultiplayerReady;
			connectingCanvas.gameObject.SetActive(false);
			gameplayCanvas.gameObject.SetActive(true);
			gameplay.StartGame(State.MultiplayerReady);
		};
		networkingLayer.OnRoomNotReady += (object sender, EventArgs e) => {
			gameState = State.MultiplayerConnecting;
			gameplay.gameState = State.MultiplayerConnecting;
			connectingCanvas.gameObject.SetActive(true);
			gameplayCanvas.gameObject.SetActive(false);
		};
	}
	void ShowMenu(){
		gameplay.ResetGame();
		gameState = State.Menu;
		menuCanvas.gameObject.SetActive(true);
		connectingCanvas.gameObject.SetActive(false);
		gameplayCanvas.gameObject.SetActive(false);
	}

	void Update(){
		if(gameState == State.Menu){
			if(Input.GetKeyUp(KeyCode.Alpha1)){
				menuCanvas.gameObject.SetActive(false);
				gameState = State.OnePlayer;
				gameplay.StartGame(gameState);
				gameplayCanvas.gameObject.SetActive(true);
			} else if(Input.GetKeyUp(KeyCode.Alpha2)){
				menuCanvas.gameObject.SetActive(false);
				gameState = State.TwoPlayers;
				gameplay.StartGame(gameState);
				gameplayCanvas.gameObject.SetActive(true);
			} else if(Input.GetKeyUp(KeyCode.Alpha3)){
				gameState = State.MultiplayerConnecting;
				menuCanvas.gameObject.SetActive(false);
				connectingCanvas.gameObject.SetActive(true);
				networkingLayer.FindGameAndJoin();
			}
		} else {
			if(Input.GetKeyUp(KeyCode.Escape)){
				if(gameState == State.MultiplayerReady || gameState == State.MultiplayerConnecting){
					networkingLayer.LeaveGame();
				}
				ShowMenu();
			}
		}
	}
}
