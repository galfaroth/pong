﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void WonEvent(object sender, EventArgs e);

public class Player : MonoBehaviour, IPunObservable {
	#region ----Public Enums----
	public enum Side{
		None = 0,
		Left,
		Right
	}
	#endregion

	#region ----Properties----
    public event WonEvent OnWon; //Public Event fired when a player reaches MaxScore
	public PlayerInput input;
	public int score;

	[SerializeField]
	GameplaySettings gameplaySettings;
	#endregion
	
	#region ----Public Methods----
	public void IncrementScore(){
		score += 1;
		if(score >= gameplaySettings.maxScore){
			OnWon(this,EventArgs.Empty);
		}
	}
	#endregion

	#region ----Networking Layer----
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if (stream.isWriting){ //We own this player: send the others our data
			stream.SendNext(score);
		} else { //Network player, receive data
			this.score = (int)stream.ReceiveNext();
		}
	}
	public void OnOwnershipRequest(object[] viewAndPlayer){
		PhotonView view = viewAndPlayer[0] as PhotonView;
		PhotonPlayer requestingPlayer = viewAndPlayer[1] as PhotonPlayer;
	
		Debug.Log("OnOwnershipRequest(): Player " + requestingPlayer + " requests ownership of: " + view + ".");
		view.TransferOwnership(requestingPlayer.ID);
	}
	#endregion
}
